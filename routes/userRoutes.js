const express = require('express');
const router = express.Router();
const userControllers = require('../controllers/userControllers')

// create a user route
router.post('/', userControllers.createUserController );

// retrieving all user route
router.get ('/', userControllers.getAllUserController);


////////


// retrieving single user route
router.get('/getSingleUser/:id', userControllers.getSingleUserController);

// updating single user's status route
router.put('/updateUserUsername/:id', userControllers.updateUserUsernameController);


////////

module.exports = router;


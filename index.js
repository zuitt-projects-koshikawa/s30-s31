// Note : all packages/ modules should be required at the top of the file to avoid tampering or errors.

const express = require('express');

// Mongoose is a package that uses ODM or object document mapper. It allows us to translate our JS objects into our database documents for MongoDB. It allows connection and easier manipulation of our documents in MongoDB.
const mongoose = require('mongoose');

const taskRoutes = require('./routes/taskRoutes')
const userRoutes = require('./routes/userRoutes')

const port = 4000;

const app = express();

/*
	Syntax:
		mongoose.connect("<connectionStringFromMongoDBAtlas>", {
			useNewURLParser: true,
			useUnifiedTopology: true
		})
*/


mongoose.connect("mongodb+srv://admin_koshikawa:admin169@batch-169.df9yo.mongodb.net/task169?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true,
});

// Create notification if the connection to the db is successful or not

let db = mongoose.connection;

// We add this so that when db has a connection error, we will show the connection error both in the terminal and in the browser for our client
	// bind shows 2 argument the location and its message
db.on('error', console.error.bind(console, 'Connection Error'));

// Once the connection is open and successful, we will output a message in the terminal
	// on and once are events. its used so when something happens you something is called.
	// bad auth in console means wrong password.

db.once('open', () => console.log('Connected to MongoDB'));

// middlewares
app.use(express.json() );

// our server will use a middleware to group all task routes under /tasks
// All the endpoints in taskRoutes will start with /tasks
app.use('/tasks', taskRoutes);
app.use('/users', userRoutes);


app.listen(port, () => console.log(`Server is running at port ${port}`))
